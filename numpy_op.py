# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 20:23:51 2016

@author: cc

使用 numpy 基本运算方法

http://docs.scipy.org/doc/numpy/user/quickstart.html ＃Basic Operations

"""

import numpy as np

data1 = np.random.random((2,3))
data2 = np.random.random((2,3))
data3 = np.random.random((3,2))
print data1
print data2
print data3

print "加"
print data1+data2
print np.add(data1,data2)
print "减"
print data1-data2
print "求最小值"
print np.min(data1)
print "求最大值"
print np.max(data1)
print "求和"
print np.sum(data1)
print "数值*2"
print data1*2
print "2次方"
print data1**2
print "数值比较"
print data1<1
print "矩阵数值比较"
print data1<data2
print "矩阵数值相乘"
print data1*data2
print "data1.dot(data3) 点乘"
print data1.dot(data3)
print np.dot(data1,data3)

print "data4"
data4 = np.random.random((5,4))
print data4
print "取出第2维值"
print data4[1] #第2维 从0起 1就是第二
print "取出倒数第1维值"
print data4[-1]
print "取出第1至第3维值"
print data4[0:3] 
print "取出一个值"
print data4[1,1] #第2维 第2个值
print "第二到第三行，每行的第四个数"
print data4[1:3,3]
print "每行的倒数第2个数"
print data4[:,-2]
