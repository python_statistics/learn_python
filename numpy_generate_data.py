# -*- coding: utf-8 -*-
"""
Created on Sun Sep 11 20:23:51 2016

@author: cc

使用 numpy 生成数据

http://docs.scipy.org/doc/numpy/user/quickstart.html ＃Array Creation

"""
import numpy as np
from numpy import pi

print "给定数值生成矩阵"
data = np.array(
    [
        [ 1., 0., 3.],
        [ 0., 1., 2.],
        [ 0., 1., 2.]
    ]
   )

print data.ndim #秩
print data.shape # 几＊几
print data.size # 元素个数
print data.dtype #元素类型
print data.itemsize #每个元素几Byte

print "全0"
zero_data = np.zeros([3,3])
print zero_data

print "全1"
one_data = np.ones([3,3])
print one_data

print "10到30 步进为5"
range_data = np.arange( 10, 30, 5 ) #from 10 to 30 step 5
print range_data

print "0到2 中间填充9个数"
linespace = np.linspace( 0, 2, 9 ) #9 numbers from 0 to 2
print linespace

print "0到2pi 之间的100个数 常用"
sin_base = np.linspace( 0, 2*pi, 100 )  
print sin_base

print "1到100的矩阵 然后转换为10＊10的矩阵"
reshape_line = np.arange(100)
print reshape_line
reshape_line = reshape_line.reshape([10,10])
print reshape_line

print "生成2＊3的随机数矩阵"
randam_data  = np.random.random((2,3))
print randam_data